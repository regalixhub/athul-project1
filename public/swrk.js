const staticCacheName = "site-static";
const assets = ["/", "/index.html"];

// install service worker

self.addEventListener("install", evt => {
  console.log("service worker has been installed changed");

  caches.open(staticCacheName).then(caches => {
    caches.add;
  });
});

//  activate service worker

self.addEventListener("activate", evt => {
  console.log("service worker has been activated changed");
});

// fetch event

self.addEventListener("fetch", evt => {
  console.log("fetch event", evt);
});

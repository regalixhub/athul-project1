// register service worker

if ("serviceWorker" in navigator) {
  navigator.serviceWorker
    .register("swrk.js")
    .then(reg => console.log("service worker register", reg))
    .catch(err => console.log("service worker not registered", err));
}
